'use strict'

require.config({
    baseUrl: 'assets/resources',
    paths: {
        'jquery': 'jquery-2.1.1/jquery-2.1.1',
        '_': 'underscore-1.7.0/underscore',
        'backbone': 'backbone.marionette/backbone',
        'marionette': 'backbone.marionette/backbone/marionette',
        'fb-js-sdk': '../fb-js-sdk',
        'bootstrap': 'bootstrap-3.2.0-dist/bootstrap'
    }
});
require(['fb-js-sdk']);
