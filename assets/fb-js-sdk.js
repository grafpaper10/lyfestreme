window.fbAsyncInit = function() {
    FB.init({
        appId: '316001955267714',
        xfbml: true,
        version: 'v2.2'
    });
    FB.login(function() {
        // FB.api('/me/message', 'post', {
        //     message: 'Hello, world!'
        // });
        FB.api('/me', function(response) {
            if (response && !response.error) {
                /* handle the result */
                console.log(response);
            }
        });
    }, {
        scope: ['publish_actions', 'manage_friendlists', 'user_friends', 'public_profile']
    });
};

(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
